<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!empty(Auth::user()->is_employee)) {
                Session::put('permission', 'employee');
                $name = Route::currentRouteName();
                $aRouteName = explode('.', $name);
                if (count($aRouteName) >= 2) {
                    if ($aRouteName[0] == 'user-management' && ($aRouteName[1] == 'edit' || $aRouteName[1] == 'update')) {
                        if (!empty(Route::current()->parameters['user_management']) && Auth::user()->id == Route::current()->parameters['user_management']) {
                            return $next($request);
                        }
                    }
                } else if ($name == 'dashboard') {
                    return $next($request);
                }

                // 403
                return redirect()->intended('/dashboard');
            }
            return $next($request);
        });
    }
}
