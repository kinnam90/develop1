<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use App\Country;
use App\Department;
use App\Division;
use Response;

class UserManagementController extends Controller
{
       /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user-management';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');     
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Auth::user()->is_employee); 
        $users = User::paginate(5); 
        $employees = DB::table('users')->where('is_employee',1)
        ->leftJoin('department', 'users.department_id', '=', 'department.id')
        ->leftJoin('country', 'users.country_id', '=', 'country.id')
        ->leftJoin('division', 'users.division_id', '=', 'division.id')
        ->select('users.*', 'department.name as department_name', 'department.id as department_id', 'division.name as division_name', 'division.id as division_id')
        ->paginate(5);
// dd($employees);
        return view('users-mgmt/index', ['users' => $users]); 
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $departments = Department::all();
        $divisions = Division::all();
        return view('users-mgmt/create',['countries' => $countries,
        'departments' => $departments, 'divisions' => $divisions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
        // dd($request);    
        // upload images
        // dd($request->file('picture'));
       
        if ($request->hasFile('picture')) {
 
            $request->picture->move(public_path() . '/uploads/', strtotime("now") . '-' . $request->picture->getClientOriginalName());
            $input['picture'] = strtotime("now") . '-' . $request->picture->getClientOriginalName();

        }
        $keys = [ 'address', 'country_id', 'username', 'age','picture',
        'birthdate', 'date_hired', 'department_id', 'division_id', 'password'];
        $input = $this->createQueryInput($keys, $request);
      
        $input['is_employee'] = 1;
        $input['password'] = bcrypt('123456');
        $input['email'] = '123456@gmail.com';
         User::create([
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'age' => $request['age'],
            // 'lastname' => $request['lastname'],
            'address' => $request['address'],
             'picture' => $input['picture'],
            'country_id' => $request['country_id'],
            'birthdate' => $request['birthdate'],
            'department_id' => $request['department_id'],
            'division_id' => $request['division_id'],
                        
        ]);

        return redirect()->intended('/user-management');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        // Redirect to user list if updating user wasn't existed
        if (!$user) {
            return redirect()->intended('/user-management');
        }

        $countries = Country::all();
        $departments = Department::all();
        $divisions = Division::all();

        return view('users-mgmt/edit', ['user' => $user,'countries' => $countries,
        'departments' => $departments, 'divisions' => $divisions]);
    }
     /**
     * Load image resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function load($name) {
         $path = storage_path().'/app/avatar/'.$name;
        if (file_exists($path)) {
            return Response::download($path);
        }
    }
  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validateInput($request);
        $keys = [ 'address', 'country_id' 
        , 'birthdate', 'date_hired', 'department_id', 'department_id', 'division_id'];
        $input = $this->createQueryInput($keys, $request);
        $input['is_employee'] = 1;
        if ($request->file('picture')) {
            $path = $request->file('picture')->store('public');
            $input['picture'] = $path;
        }

        User::where('id', $id)
            ->update($input);
        $constraints = [
            'username' => 'required|max:20',
            'address' => $request['address'],
            'country_id' => $request['country_id'],
            'birthdate' => $request['birthdate'],
            'department_id' => $request['department_id'],
            'division_id' => $request['division_id'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'age' => $request['age'],

            // 'firstname'=> 'required|max:60',
            // 'lastname' => 'required|max:60'
            ];
        $input = [
            'username' => $request['username'],
            'address' => $request['address'],
            'country_id' => $request['country_id'],
            'birthdate' => $request['birthdate'],
            'department_id' => $request['department_id'],
            'division_id' => $request['division_id'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'age' => $request['age'],
            // 'firstname' => $request['firstname'],
            // 'lastname' => $request['lastname']
        ];
        if ($request['password'] != null && strlen($request['password']) > 0) {
            $constraints['password'] = 'required|min:6|confirmed';
            $input['password'] =  bcrypt($request['password']);
        }
       
        
        return redirect()->intended('/user-management');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();
         return redirect()->intended('/user-management');
    }

    
    public function deletesingle(Request $request,$id)
    {
        User::where('id', $id)->delete();
         return redirect()->intended('/user-management');
    }

    /**
     * Search user from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'username' => $request['username'],
            // 'firstname' => $request['firstname'],
            // 'lastname' => $request['lastname'],
            'department' => $request['department']
            ];

       $users = $this->doSearchingQuery($constraints);
       return view('users-mgmt/index', ['users' => $users, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = User::query();
        $query = DB::table('users')->where('is_employee',1)
        // ->leftJoin('city', 'users.city_id', '=', 'city.id')
        ->leftJoin('department', 'users.department_id', '=', 'department.id')
        // ->leftJoin('state', 'users.state_id', '=', 'state.id')
        ->leftJoin('country', 'users.country_id', '=', 'country.id')
        ->leftJoin('division', 'users.division_id', '=', 'division.id')
        ->select('users.firstname as employee_name', 'users.*','department.name as department_name', 'department.id as department_id', 'division.name as division_name', 'division.id as division_id');
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }

    
      
    private function validateInput($request) {
        // die('OK');
        $this->validate($request, [
        'username' => 'required|max:20',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'confirmed',
        'address' => 'required|max:120',
        'country_id' => 'required',
        // 'birthdate' => 'required',
        // 'date_hired' => 'required',
        'department_id' => 'required',
        'division_id' => 'required',
        // 'picture' => 'required',


        // 'firstname' => 'required|max:60',
        // 'lastname' => 'required|max:60'
    ]);
    }

            
    public function resetpwd(Request $request) {
        try {
            if ($request->has('reset_all')) {
                if (!$request->has('list-reset')) {
                    throw new \Exception('Please select user want remove!');
                }
                DB::beginTransaction();
                $listReset = $request['list-reset'];
                foreach ($listReset as $keyDel => $valDel) {
                    $user = User::find($valDel); 
                    if (empty($user) || !$user->update(['password' => bcrypt($user->username)])) {
                        throw new \Exception('Reset list user FAILED!');
                    } else {
                        //THANH CONG THÌ VÀO ĐÂY ĐÚNG KHÔNG?
                        $this->sendMail($user);
                    }
                    
                }
                DB::commit();
                return redirect()->back()->with('success', 'Reset user success!');
            }
            return redirect()->back();
        } catch(\Exception $ex) {
            DB::rollback();
            return redirect()->back()->with('error', $ex->getMessage());
        }
        // viet ntn reset sao đc k có token truyền vào ?? gio phai lam sao bac laravel k có kiểu resetall đâu reset tưng cái thôi vậy bác giúp e với
        // reset từng cái hả vâng mới cả xem cho e sao e toàn bị lỗi linh tinh ví dụ như add nhân viên mới no k ra ảnh vs 1 số thông tim như 
    }

    private function sendMail($user)
    {
        //DOC TAI LIEU CUA Laravel coi nó send mail như thế nào rồi viết ở đây với thông tin chứa trong biến $user
        //Em muon gui kem link de no click vo link do moi reset hả?
        //Nếu muốn gửi link thì ở đây em tạo 1 link theo ý em muốn rồi gửi vào mail thôi
    }

    private function createQueryInput($keys, $request) {
        $queryInput = [];
        for($i = 0; $i < sizeof($keys); $i++) {
            $key = $keys[$i];
            $queryInput[$key] = $request[$key];
        }

        return $queryInput;
    }
}

