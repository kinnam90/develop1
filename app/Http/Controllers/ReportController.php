<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Employee;
use App\User;
use Excel;
use Illuminate\Support\Facades\DB;  
use Auth;
use PDF;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index() {
        date_default_timezone_set('asia/ho_chi_minh');
        $format = 'Y/m/d';
        $now = date($format);
        $to = date($format, strtotime("+30 days"));
        $constraints = [
            'from' => $now,
            'to' => $to
        ];

        $users = $this->getHireduser($constraints);

        // dd($users);
        return view('system-mgmt.report.index', ['users' => $users, 'searchingVals' => $constraints]);
    }

    public function exportExcel(Request $request) {
        $this->prepareExportingData($request)->export('xlsx');
        redirect()->intended('system-management/report');
    }

    public function exportPDF(Request $request) {
         $constraints = [
            'from' => $request['from'],
            'to' => $request['to']
        ];
        $users = $this->getExportingData($constraints);
        $pdf = PDF::loadView('system-mgmt/report/pdf', ['user' => $users, 'searchingVals' => $constraints]);
        return $pdf->download('report_from_'. $request['from'].'_to_'.$request['to'].'pdf');
        // return view('system-mgmt/report/pdf', ['user' => $user, 'searchingVals' => $constraints]);
    }
    
    private function prepareExportingData($request) {
        $author = Auth::user()->username;
        $user = $this->getExportingData(['from'=> $request['from'], 'to' => $request['to']]);
        return Excel::create('report_from_'. $request['from'].'_to_'.$request['to'], function($excel) use($user, $request, $author) {

        // Set the title
        $excel->setTitle('List of hired user from '. $request['from'].' to '. $request['to']);

        // Chain the setters
        $excel->setCreator($author)
            ->setCompany('Nam_Ngo');

        // Call them separately
        $excel->setDescription('The list of hired user');

        $excel->sheet('Hired_user', function($sheet) use($user) {

        $sheet->fromArray($user);
            });
        });
    }

    public function search(Request $request) {
        $constraints = [
            'from' => $request['from'],
            'to' => $request['to']
        ];

        $users = $this->getHireduser($constraints);
        return view('system-mgmt.report.index', ['users' => $users, 'searchingVals' => $constraints]);
    }

    private function getHireduser($constraints) {
        $user = User::where('date_hired', '>=', $constraints['from'])
                        ->where('date_hired', '<=', $constraints['to'])
                        ->get();
        return $user;
    }

    private function getExportingData($constraints) {
        return DB::table('users')
        // ->leftJoin('city', 'user.city_id', '=', 'city.id')
        ->leftJoin('department', 'users.department_id', '=', 'department.id')
        ->leftJoin('country', 'users.country_id', '=', 'country.id')
        ->leftJoin('division', 'users.division_id', '=', 'division.id')
        ->select('users.username' 
        ,'users.birthdate', 'users.address', 'users.date_hired',
        'department.name as department_name', 'division.name as division_name')
        ->where('date_hired', '>=', $constraints['from'])
        ->where('date_hired', '<=', $constraints['to'])
        ->get()
        ->map(function ($item, $key) {
        return (array) $item;
        })
        ->all();
    }
}
