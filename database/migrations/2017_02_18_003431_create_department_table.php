<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('name', 60);
            // id_sub_department
            // $table->string('id_sub_department');
            $table->timestamps();
            $table->softDeletes(); 
        });


        /* chuc_vu 
            id : primary 
            name: "dev"
            id_department: int -> kinh_doanh -> 2 
            time
            delete
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department');
    }
}
