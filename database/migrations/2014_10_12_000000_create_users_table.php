<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('lastname');
            $table->string('firstname');
            $table->tinyInteger('active')->default(0);
            $table->string('address')->default(null);
            $table->string('picture');

            // $table->tinyInteger('level')->default(0);
            // $table->tinyInteger('first_log')->default(0);

            // $table->integer('city_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('age')->unsigned();
            $table->integer('division_id')->unsigned();
            $table->integer('department_id')->unsigned();
            // id_chuc_vu 

            
            $table->foreign('country_id')->references('id')->on('country');
            $table->foreign('department_id')->references('id')->on('department');
            $table->foreign('division_id')->references('id')->on('division');

            // $table->char('zip');
            
            $table->date('birthdate');
            $table->date('date_hired');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
